package com.devcamp.relationshipcrud.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.devcamp.relationshipcrud.entity.Post;

@Repository
public interface PostRepository extends JpaRepository<Post, Long> {

}