package com.devcamp.relationshipcrud.controller;

import java.util.ArrayList;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.relationshipcrud.entity.Post;
import com.devcamp.relationshipcrud.entity.Tag;


import com.devcamp.relationshipcrud.repository.PostRepository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class PostTagController {
    @Autowired
    private PostTagController tagRepository;

    @Autowired
    private PostRepository postRepository;
    
	@GetMapping("/createPost")
	public ResponseEntity<Post> createPost() {
        try {
        	//xoá hết dữ liệu để test
        	postRepository.deleteAllInBatch();
        	postRepository.deleteAllInBatch();        	
        	// Create a Post
            Post post = new Post("Ví dụ many to many sample bằng spring boot title"
            		,"Ví dụ many to many sample bằng spring boot description"
            		,"Ví dụ many to many sample bằng spring boot content");
            // Create two tags
            Tag tag1 = new Tag("Spring Boot");
            Tag tag2 = new Tag("JPA");
            // Add tag references in the post
            post.getTags().add(tag1);
            post.getTags().add(tag2);            
         // Add post reference in the tags
            tag1.getPosts().add(post);
            tag2.getPosts().add(post);
            Post post2 = postRepository.save(post);        	
            return new ResponseEntity<>(post2, HttpStatus.CREATED);
        } catch (Exception e) {
        	return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
	@GetMapping("/getPosts")
	public ResponseEntity<List<Post>> getAllpost() {
        try {
            List<Post> post = new ArrayList<Post>();

            postRepository.findAll().forEach(post::add);

            return new ResponseEntity<>(post, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
